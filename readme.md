@credit to https://github.com/andersao/l5-repository

1. Install `composer require tonoslfx/laravel-repository-generator`

2. Update `composer update` if necessary

3. Add service provider in config/app.php `Tonoslfx\RepositoryGenerator\RepositoryServiceProvider::class`

4. Every new repository file requires binding. In your `AppServiceProvider.php` copy the following code 

```public function boot() {
    $this->app->bind('App\Repository\Contracts\{Replace}Interface', 'App\Repository\Eloquent\{Replace}');
}```

5. Then in your controller `use App\Repository\Contracts\{Replace}Interface`

##TO-DO

Allow user to define `namespace` folder name