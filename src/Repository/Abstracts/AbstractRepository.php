<?php

namespace sgit\RepositoryGenerator\Repository\Abstracts;

use Exception;
use sgit\RepositoryGenerator\Repository\Contracts\RepositoryInterface;

/**
* @credit https://github.com/andersao/l5-repository
*/
abstract class AbstractRepository implements RepositoryInterface
{

    protected $entity;

    /**
     * AbstractRepository constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {

        $this->entity = $this->resolveEntity();

    }

    public function all(array $columns = ['*'])
    {

        return $this->entity->get($columns);

    }

    public function paginate(int $limit = 30, array $columns = ['*'], $method = 'paginate')
    {

        return $this->entity->{$method}($limit, $columns);


    }

    public function simplePaginate(int $limit = 30, array $columns = ['*'])
    {
        return $this->paginate($limit, $columns, 'simplePaginate');
    }

    public function find($id, array $columns = ['*'])
    {

        return $this->entity->findOrFail($id, $columns);

    }

    public function findByField($field, $value, array $columns = ['*'])
    {

        return $this->entity->where($field, '=', $value)->get($columns);

    }

    public function findWhere(array $where, array $columns = ['*'])
    {

        $this->applyConditions($where);

        return $this->entity->get($columns);

    }

    public function findWhereIn($field, array $values, array $columns = ['*'])
    {

        return $this->entity->whereIn($field, $values)->get($columns);

    }

    public function findWhereNotIn($field, array $values, array $columns = ['*'])
    {

        return $this->entity->whereNotIn($field, $values)->get($columns);
    }

    public function create(array $attributes)
    {

        return $this->entity->create($attributes);
    }

    public function update(array $attributes, $id)
    {

        $model = $this->entity->findOrFail($id);
        $model->fill($attributes);
        $model->save();

    }

    public function updateOrCreate(array $attributes, array $values = [])
    {

        $this->entity->updateOrCreate($attributes, $values);

    }

    public function delete(int $id)
    {

        return $this->find($id)->delete();


    }

    public function deleteWhere(array $where)
    {

        $this->applyConditions($where);

        return $this->entity->delete();


    }

    public function has($relation)
    {

        $this->entity = $this->entity->has($relation);

        return $this;


    }


    public function orderBy($column, $direction = 'asc')
    {

        $this->entity = $this->entity->orderBy($column, $direction);

        return $this;

    }

    public function with($relations)
    {

        $this->entity = $this->entity->with($relations);

        return $this;


    }

    public function whereHas($relation, $closure)
    {

        $this->entity = $this->entity->whereHas($relation, $closure);

        return $this;

    }

    public function withCount($relations)
    {

        $this->entity = $this->entity->withCount($relations);

        return $this;

    }

    public function firstOrNew(array $attributes = [])
    {

        return $this->entity->firstOrNew($attributes);

    }

    public function firstOrCreate(array $attributes = [])
    {

        return $this->entity->firstOrCreate($attributes);

    }


    /**
     * Entity method must be provided in every repository
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws Exception
     */
    protected function resolveEntity()
    {

        if (!method_exists($this, 'entity')) {
            throw new Exception('No entity defined in App\Repository\Eloquent\{Model}');
        }

        return app($this->entity());

    }

    /**
     * Applies the given where conditions to the model.
     *
     * @param array $where
     *
     * @return void
     */
    protected function applyConditions(array $where)
    {

        foreach ($where as $field => $value) {

            if (is_array($value)) {

                list($field, $condition, $val) = $value;

                $this->entity = $this->entity->where($field, $condition, $val);

            } else {

                $this->entity = $this->entity->where($field, '=', $value);

            }

        }

    }


}
