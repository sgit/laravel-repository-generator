<?php

namespace sgit\RepositoryGenerator\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class RepositoryGeneratorCommand extends GeneratorCommand
{

    protected $type = 'Repository';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:repository';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository file';


    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected function getStub()
    {
        // TODO: Implement getStub() method.
    }

    public function myNamespace()
    {

        return config('repository.namespace', 'Repository');

    }

    public function getStubs()
    {

        return [
            [
                'namespace' => 'Contracts',
                'stub' => __DIR__ . '/../stubs/entity.interface.stub',
                'interface' => true,
                'generateFile' => true
            ],
            [
                'namespace' => 'Eloquent',
                'stub' => __DIR__ . '/../stubs/eloquent.class.stub',
                'interface' => false,
                'generateFile' => true
            ],

        ];

    }


    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the model repository'],
        ];
    }

    /**
     * Build the class with the given name.
     *
     * @param $stub
     * @param $fileName
     *
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function buildClassMe($stub, $fileName)
    {
        $stub = $this->files->get($stub);

        return $this->replaceNamespace($stub, $fileName)->replaceClass($stub, $fileName);

    }

    /**
     * @param $path
     * @param $stubFile
     * @param $fileName
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function _generateFile($path, $stubFile, $fileName)
    {

        $this->makeDirectory($path);


        $this->files->put($path, $this->buildClassMe($stubFile, $fileName));


    }


    /**
     * @return bool|null
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {

        $inputName = ucfirst($this->getNameInput());

        if (file_exists($this->laravel['path'] . '/Repository/Eloquent/' . $inputName . '.php')) {

            $this->error('Model ' . $this->type . ' already exists!');

            return false;
        }


        foreach ($this->getStubs() as $stub) {

            $fileName = $inputName;

            if ($stub['interface']) {
                $fileName = $fileName . 'Interface';
            }

            $filePath = $this->qualifyClass('Repository\\' . $stub['namespace'] . '\\' . $fileName);

            $this->_generateFile($this->getPath($filePath), $stub['stub'], $fileName);

        }

        $this->info($this->type . ' created successfully.');


    }

}
