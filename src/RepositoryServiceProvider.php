<?php

namespace sgit\RepositoryGenerator;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        
        $this->commands([
            Commands\RepositoryGeneratorCommand::class
        ]);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->publishes([
            __DIR__.'/config/repository.php' => config_path('repository.php')
        ], 'repository');

    }
}
